package com.example.teamproject5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Spinner;

public class DistActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dist);

        Spinner spin1 = findViewById(R.id.spinnerFrom);
        ArrayAdapter<CharSequence> adapt1 = ArrayAdapter.createFromResource(this,R.array.distance, android.R.layout.simple_spinner_item);
        adapt1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin1.setAdapter(adapt1);
        spin1.setOnItemSelectedListener(this);

        Spinner spin2 = findViewById(R.id.spinnerTo);
        ArrayAdapter<CharSequence> adapt2 = ArrayAdapter.createFromResource(this,R.array.distance, android.R.layout.simple_spinner_item);
        spin2.setAdapter(adapt2);
        spin2.setOnItemSelectedListener(this);

        Button btnT;
        final TextView answerT;
        btnT = (Button) findViewById(R.id.button);
        answerT = (TextView) findViewById(R.id.answerBox);

        btnT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Spinner spin1 = (Spinner) findViewById(R.id.spinnerFrom);
                String fromDist = spin1.getSelectedItem().toString();

                Spinner spin2 = (Spinner) findViewById(R.id.spinnerTo);
                String toDist = spin2.getSelectedItem().toString();

                EditText initVal = findViewById(R.id.initialValue);
                String initDist = initVal.getText().toString();

                double value = 0;
                if(!"".equals(initDist)){
                    value=Double.parseDouble(initDist);
                }

                if(fromDist.equals(toDist) && fromDist.equals("Kilometer")){
                    answerT.setText(initDist);
                    answerT.append("km");
                }
                else if(fromDist.equals(toDist) && fromDist.equals("Meter")){
                    answerT.setText(initDist);
                    answerT.append("m");
                }
                else if(fromDist.equals(toDist) && fromDist.equals("Centimeter")){
                    answerT.setText(initDist);
                    answerT.append("cm");
                }
                else if(fromDist.equals(toDist) && fromDist.equals("Mile")){
                    answerT.setText(initDist);
                    answerT.append("mi");
                }
                else if(fromDist.equals(toDist) && fromDist.equals("Foot")){
                    answerT.setText(initDist);
                    answerT.append("ft");
                }
                else if(fromDist.equals(toDist) && fromDist.equals("Inch")){
                    answerT.setText(initDist);
                    answerT.append("in");
                }
                else if(fromDist.equals("Kilometer")&&toDist.equals("Meter")){
                    double resultKmM = (value * 1000);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("m");
                }
                else if(fromDist.equals("Kilometer")&&toDist.equals("Centimeter")){
                    double resultKmM = (value * 100000);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("cm");
                }
                else if(fromDist.equals("Kilometer")&&toDist.equals("Mile")){
                    double resultKmM = (value * 0.621371);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("mi");
                }
                else if(fromDist.equals("Kilometer")&&toDist.equals("Foot")){
                    double resultKmM = (value * 3280.84);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("ft");
                }
                else if(fromDist.equals("Kilometer")&&toDist.equals("Inch")){
                    double resultKmM = (value * 39370.1);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("in");
                }
                else if(fromDist.equals("Meter")&&toDist.equals("Kilometer")){
                    double resultKmM = (value * 0.001);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("km");
                }
                else if(fromDist.equals("Meter")&&toDist.equals("Centimeter")){
                    double resultKmM = (value * 100);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("cm");
                }
                else if(fromDist.equals("Meter")&&toDist.equals("Mile")){
                    double resultKmM = (value * 0.000621371);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("mi");
                }
                else if(fromDist.equals("Meter")&&toDist.equals("Foot")){
                    double resultKmM = (value * 3.28084);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("ft");
                }
                else if(fromDist.equals("Meter")&&toDist.equals("Inch")){
                    double resultKmM = (value * 39.3701);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("in");
                }
                else if(fromDist.equals("Centimeter")&&toDist.equals("Kilometer")){
                    double resultKmM = (value * 0.00001);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("km");
                }
                else if(fromDist.equals("Centimeter")&&toDist.equals("Meter")){
                    double resultKmM = (value * 0.01);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("m");
                }
                else if(fromDist.equals("Centimeter")&&toDist.equals("Mile")){
                    double resultKmM = (value * 0.00000621371);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("mi");
                }
                else if(fromDist.equals("Centimeter")&&toDist.equals("Foot")){
                    double resultKmM = (value * 0.0328084);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("ft");
                }
                else if(fromDist.equals("Centimeter")&&toDist.equals("Inch")){
                    double resultKmM = (value * 0.393701);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("in");
                }
                else if(fromDist.equals("Mile")&&toDist.equals("Kilometer")){
                    double resultKmM = (value * 1.60934);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("km");
                }
                else if(fromDist.equals("Mile")&&toDist.equals("Meter")){
                    double resultKmM = (value * 1609.34);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("m");
                }
                else if(fromDist.equals("Mile")&&toDist.equals("Foot")){
                    double resultKmM = (value * 5280);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("ft");
                }
                else if(fromDist.equals("Mile")&&toDist.equals("Inch")){
                    double resultKmM = (value * 63360);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("in");
                }
                else if(fromDist.equals("Mile")&&toDist.equals("Centimeter")){
                    double resultKmM = (value * 160934);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("cm");
                }
                else if(fromDist.equals("Foot")&&toDist.equals("Kilometer")){
                    double resultKmM = (value * 0.0003048);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("km");
                }
                else if(fromDist.equals("Foot")&&toDist.equals("Meter")){
                    double resultKmM = (value * 0.3048);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("m");
                }
                else if(fromDist.equals("Foot")&&toDist.equals("Centimeter")){
                    double resultKmM = (value * 30.48);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("cm");
                }
                else if(fromDist.equals("Foot")&&toDist.equals("Mile")){
                    double resultKmM = (value * 0.000189394);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("mi");
                }
                else if(fromDist.equals("Foot")&&toDist.equals("Inch")){
                    double resultKmM = (value * 12);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("in");
                }
                else if(fromDist.equals("Inch")&&toDist.equals("Kilometer")){
                    double resultKmM = (value * 0.0000254);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("km");
                }
                else if(fromDist.equals("Inch")&&toDist.equals("Meter")){
                    double resultKmM = (value * 0.0254);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("m");
                }
                else if(fromDist.equals("Inch")&&toDist.equals("Centimeter")){
                    double resultKmM = (value * 2.54);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("cm");
                }
                else if(fromDist.equals("Inch")&&toDist.equals("Mile")){
                    double resultKmM = (value * 0.000015783);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("mi");
                }
                else if(fromDist.equals("Inch")&&toDist.equals("Foot")){
                    double resultKmM = (value * 0.0833333);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("ft");
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
