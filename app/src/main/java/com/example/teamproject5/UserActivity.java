package com.example.teamproject5;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class UserActivity extends AppCompatActivity {

    //Assaign variables to views Part 1
    TextView txtResult;
    Spinner spinResult;
    EditText txtUserInput;
    Button btnConvert;
    Button btnAdd;
    Button btnSave;
    EditText editTxt1;
    EditText editTxt1Num;
    EditText editTxt2;
    EditText editTxt2Num;
    ImageView imageView5;

    //Variables unassaigned to views
    private dbManager dbManage;
    float ratio;


//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        //Assaign variables to views part 2
        txtResult = (TextView) findViewById(R.id.txtResult);
        spinResult = (Spinner) findViewById(R.id.spinResult);
        txtUserInput = (EditText) findViewById(R.id.txtUserInput);
        btnConvert = (Button) findViewById(R.id.btnConvert);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnSave = (Button) findViewById(R.id.btnSave);
        editTxt1 = (EditText) findViewById(R.id.editTxt1);
        editTxt1Num = (EditText) findViewById(R.id.editTxt1Num);
        editTxt2 = (EditText) findViewById(R.id.editTxt2);
        editTxt2Num = (EditText) findViewById(R.id.editTxt2Num);
        imageView5 = (ImageView) findViewById(R.id.imageView5);

        //Variables not assaigned to view. Hope that i can setup a different database table_name?
        dbManage = new dbManager(this);


        //Assaign sql list to spinner
        ListToSpinner();



        //Upon Create i will hide the below option until btnAdd is clicked
        showBottomStuff(false);


        //Hide bottom stuff
        btnAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
               showBottomStuff(true);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                if(!editTxt1.getText().toString().isEmpty() && !editTxt2.getText().toString().isEmpty() && !editTxt1Num.getText().toString().isEmpty() && !editTxt1Num.getText().toString().isEmpty()) {
                    //Adds it to the SQL
                    float a = Float.valueOf(editTxt1Num.getText().toString());
                    float b = Float.valueOf(editTxt2Num.getText().toString());
                    float ab = b / a;
                    String temp1 = editTxt1.getText().toString();
                    String temp2 = editTxt2.getText().toString();
                    AddData(temp2, ab, temp1);

                    //Update spinner
                    ListToSpinner();

                    //Upon Create i will hide the below option until btnAdd is clicked
                    showBottomStuff(false);
                }
            }
        });



        btnConvert.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //Takes in informaion of txtUserInput and checks if its a number
                //Temporarly create Cursor and find which item the userselected.
                //Send that positionID to conversionInWork and the user input

                    if(spinResult != null && spinResult.getSelectedItem() != null) {
                        Cursor cursor = dbManage.getData();
                        cursor.moveToPosition(spinResult.getSelectedItemPosition());
                        float tempRatio = cursor.getFloat(2);
                        if (!txtUserInput.getText().toString().isEmpty()) {
                            float userInput = Float.valueOf(txtUserInput.getText().toString());
                            txtResult.setText(Float.toString(tempRatio * userInput));
                        }
                    }

            }
        });




    }



    //Simplifies toast message because i use way too much.
    private void toastMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }


    public void showBottomStuff(boolean x) {
        if(x){
            editTxt1.setVisibility(View.VISIBLE);
            editTxt1Num.setVisibility(View.VISIBLE);
            editTxt2.setVisibility(View.VISIBLE);
            editTxt2Num.setVisibility(View.VISIBLE);
            imageView5.setVisibility(View.VISIBLE);
        } else {
            editTxt1.setVisibility(View.INVISIBLE);
            editTxt1Num.setVisibility(View.INVISIBLE);
            editTxt2.setVisibility(View.INVISIBLE);
            editTxt2Num.setVisibility(View.INVISIBLE);
            imageView5.setVisibility(View.INVISIBLE);
        }
    }


    //-----------------------------BELOW IS ALL DATABASE FUNCTIONS--------------------------------------

    //Calls database managers function to add data to sql list.
    public void AddData(String var1, float ratioIn, String var2){
        boolean work = dbManage.addData(var1, ratioIn, var2);

        if(work)
            toastMessage("Note Added!");
        else
            toastMessage("Not not added!");

    }

    //Deletes data functino from sql and refeshes list
    public void DeleteData(String ID){
        int integer = Integer.parseInt(ID);
        boolean work = dbManage.deleteData(integer);

        if(work)
            toastMessage("Note Added!");
        else
            toastMessage("Not not added!");

    }



    //refreshes the list.
    private void ListToSpinner() {
        //Log.d(TAG, "populateListView: Displaying data in the ListView.");


        Cursor data = dbManage.getData();
        ArrayList<String> listData = new ArrayList<>();
        while(data.moveToNext()){
            String temp = "";
            temp += data.getString(1);
            temp += " ";
            temp += String.valueOf(data.getFloat(2));
            temp += " ";
            temp += data.getString(3);
            listData.add(temp);
        }

        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        spinResult.setAdapter(adapter);
        //noteList.setAdapter(adapter);
        //set it to spinner upon creation.
    }
}
