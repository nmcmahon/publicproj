package com.example.teamproject5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button Mon;
    private Button Temp;
    private Button Dist;
    private Button custom;

    //Add the three buttons
    //Add SharedPrefence for the 3 new buttons. If sharedPreference doesn't exist. Then user never made it. thus set default text to "add category"
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Mon = findViewById(R.id.Money);
        Temp = findViewById(R.id.Temperature);
        Dist = findViewById(R.id.Distance);
        custom = findViewById(R.id.Custom);

        Mon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,MoneyActivity.class);
                startActivity(i);
            }
        });

        Temp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,TempActivity.class);
                startActivity(i);
            }
        });

        Dist.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,DistActivity.class);
                startActivity(i);
            }
        });

        custom.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,UserActivity.class);
                startActivity(i);
            }
        });


    }
}
