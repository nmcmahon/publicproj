package com.example.teamproject5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Spinner;

public class TempActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);

        Spinner spin1 = findViewById(R.id.spinnerFrom);
        ArrayAdapter<CharSequence> adapt1 = ArrayAdapter.createFromResource(this,R.array.temperature, android.R.layout.simple_spinner_item);
        adapt1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin1.setAdapter(adapt1);
        spin1.setOnItemSelectedListener(this);

        Spinner spin2 = findViewById(R.id.spinnerTo);
        ArrayAdapter<CharSequence> adapt2 = ArrayAdapter.createFromResource(this,R.array.temperature, android.R.layout.simple_spinner_item);
        spin2.setAdapter(adapt2);
        spin2.setOnItemSelectedListener(this);

        Button btnT;
        final TextView answerT;
        btnT = (Button) findViewById(R.id.button);
        answerT = (TextView) findViewById(R.id.answerBox);

        btnT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Spinner spin1 = (Spinner) findViewById(R.id.spinnerFrom);
                String fromTemp = spin1.getSelectedItem().toString();

                Spinner spin2 = (Spinner) findViewById(R.id.spinnerTo);
                String toTemp = spin2.getSelectedItem().toString();

                EditText initVal = findViewById(R.id.initialValue);
                String initTemp = initVal.getText().toString();

                double value = 0;
                if(!"".equals(initTemp)){
                    value=Double.parseDouble(initTemp);
                }

                if(fromTemp.equals(toTemp) && fromTemp.equals("Fahrenheit")){
                    answerT.setText(initTemp);
                    answerT.append("℉");
                }
                else if(fromTemp.equals(toTemp) && fromTemp.equals("Celsius")){
                    answerT.setText(initTemp);
                    answerT.append("℃");
                }
                else if(fromTemp.equals("Fahrenheit")&&toTemp.equals("Celsius")){
                    double resultFC = ((value -32)*(5.0/9.0));
                    String sDub= Double.toString(resultFC);
                    answerT.setText(sDub);
                    answerT.append("℃");
                }
                else if(fromTemp.equals("Celsius")&&toTemp.equals("Fahrenheit")){
                    double resultCF = ((value*(9.0/5.0))+32);
                    String sDub= Double.toString(resultCF);
                    answerT.setText(sDub);
                    answerT.append("℉");
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
