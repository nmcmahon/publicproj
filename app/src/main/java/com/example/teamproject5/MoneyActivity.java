package com.example.teamproject5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Spinner;

public class MoneyActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money);

        Spinner spin1 = findViewById(R.id.spinnerFrom);
        ArrayAdapter<CharSequence> adapt1 = ArrayAdapter.createFromResource(this,R.array.Money, android.R.layout.simple_spinner_item);
        adapt1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin1.setAdapter(adapt1);
        spin1.setOnItemSelectedListener(this);

        Spinner spin2 = findViewById(R.id.spinnerTo);
        ArrayAdapter<CharSequence> adapt2 = ArrayAdapter.createFromResource(this,R.array.Money, android.R.layout.simple_spinner_item);
        spin2.setAdapter(adapt2);
        spin2.setOnItemSelectedListener(this);

        Button btnT;
        final TextView answerT;
        btnT = (Button) findViewById(R.id.button);
        answerT = (TextView) findViewById(R.id.answerBox);

        btnT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Spinner spin1 = (Spinner) findViewById(R.id.spinnerFrom);
                String fromCurr = spin1.getSelectedItem().toString();

                Spinner spin2 = (Spinner) findViewById(R.id.spinnerTo);
                String toCurr = spin2.getSelectedItem().toString();

                EditText initVal = findViewById(R.id.initialValue);
                String initCurr = initVal.getText().toString();

                double value = 0;
                if(!"".equals(initCurr)){
                    value=Double.parseDouble(initCurr);
                }

                if(fromCurr.equals(toCurr) && fromCurr.equals("Dollars")){
                    answerT.setText(initCurr);
                    answerT.append("$");
                }
                else if(fromCurr.equals(toCurr) && fromCurr.equals("Euro")){
                    answerT.setText(initCurr);
                    answerT.append("€");
                }
                else if(fromCurr.equals(toCurr) && fromCurr.equals("Pound Sterling")){
                    answerT.setText(initCurr);
                    answerT.append("£");
                }
                else if(fromCurr.equals(toCurr) && fromCurr.equals("Peso")){
                    answerT.setText(initCurr);
                    answerT.append("₱");
                }
                else if(fromCurr.equals(toCurr) && fromCurr.equals("Yuan")){
                    answerT.setText(initCurr);
                    answerT.append("¥");
                }
                else if(fromCurr.equals("Dollars")&&toCurr.equals("Euro")){
                    double resultKmM = (value * 0.92);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("€");
                }
                else if(fromCurr.equals("Dollars")&&toCurr.equals("Pound Sterling")){
                    double resultKmM = (value * 0.80);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("£");
                }
                else if(fromCurr.equals("Dollars")&&toCurr.equals("Peso")){
                    double resultKmM = (value * 23.81);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("₱");
                }
                else if(fromCurr.equals("Dollars")&&toCurr.equals("Yuan")){
                    double resultKmM = (value * 7.08);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("¥");
                }
                else if(fromCurr.equals("Euro")&&toCurr.equals("Dollars")){
                    double resultKmM = (value * 1.09);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("$");
                }
                else if(fromCurr.equals("Euro")&&toCurr.equals("Pound Sterling")){
                    double resultKmM = (value * 0.87);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("£");
                }
                else if(fromCurr.equals("Euro")&&toCurr.equals("Peso")){
                    double resultKmM = (value * 25.87);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("₱");
                }
                else if(fromCurr.equals("Euro")&&toCurr.equals("Yuan")){
                    double resultKmM = (value * 7.69);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("¥");
                }
                else if(fromCurr.equals("Pound Sterling")&&toCurr.equals("Euro")){
                    double resultKmM = (value * 1.15);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("€");
                }
                else if(fromCurr.equals("Pound Sterling")&&toCurr.equals("Dollars")){
                    double resultKmM = (value * 1.25);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("$");
                }
                else if(fromCurr.equals("Pound Sterling")&&toCurr.equals("Peso")){
                    double resultKmM = (value * 29.77);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("₱");
                }
                else if(fromCurr.equals("Pound Sterling")&&toCurr.equals("Yuan")){
                    double resultKmM = (value * 8.84);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("¥");
                }
                else if(fromCurr.equals("Peso")&&toCurr.equals("Euro")){
                    double resultKmM = (value * 0.39);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("€");
                }
                else if(fromCurr.equals("Peso")&&toCurr.equals("Pound Sterling")){
                    double resultKmM = (value * 0.34);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("£");
                }
                else if(fromCurr.equals("Peso")&&toCurr.equals("Dollars")){
                    double resultKmM = (value * 0.042);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("$");
                }
                else if(fromCurr.equals("Peso")&&toCurr.equals("Yuan")){
                    double resultKmM = (value * 0.30);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("¥");
                }
                else if(fromCurr.equals("Yuan")&&toCurr.equals("Euro")){
                    double resultKmM = (value * 0.13);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("€");
                }
                else if(fromCurr.equals("Yuan")&&toCurr.equals("Pound Sterling")){
                    double resultKmM = (value * 0.11);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("£");
                }
                else if(fromCurr.equals("Yuan")&&toCurr.equals("Peso")){
                    double resultKmM = (value * 3.37);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("₱");
                }
                else if(fromCurr.equals("Yuan")&&toCurr.equals("Dollars")){
                    double resultKmM = (value * 0.14);
                    String sDub= Double.toString(resultKmM);
                    answerT.setText(sDub);
                    answerT.append("¥");
                }

            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
