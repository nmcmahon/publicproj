package com.example.teamproject5;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class dbManager extends SQLiteOpenHelper {

    //Variables
    private static final String TAG = "converter.db";

    public static final String TABLE_NAME = "category_1";//category_2 & category 3
    public static final String Col1 = "ID";
    public static final String Col2 = "Variable1";
    public static final String Col3 = "Ratio";
    public static final String Col4 = "Variable2";


    //Default constructor
    public dbManager (Context context){

        super(context, TABLE_NAME, null, 1);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable =
                "CREATE TABLE " + TABLE_NAME +
                        "("
                        + Col1 + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                        + Col2 + " TEXT, "
                        + Col3 + " REAL, "
                        + Col4 + " TEXT);"
                ;
        db.execSQL(createTable);
    }

    //Never used unless i update the app
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    //Function to add data to sql. It only needs the title and content. ID is autoincremeneted
    public boolean addData(String Variable1,float Ratio, String Variable2){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        Log.d(TAG, "addData: Adding title and content " + " to " + TABLE_NAME);

        //Adds the values. The first column SHOULD autoincrement automatically and doesn't need to be specified
        contentValues.put(Col2, Variable1);
        contentValues.put(Col3, Ratio);
        contentValues.put(Col4, Variable2);

        long result = db.insert(TABLE_NAME, null, contentValues);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    //SQL command to delete data by using ID.
    public boolean deleteData(int ID){
        SQLiteDatabase db = this.getWritableDatabase();

        Log.d(TAG, "deleteData: Adding title and content " + " to " + TABLE_NAME);



        long result = db.delete(TABLE_NAME, Col1 + "=" + ID, null);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    //Cursor function to get data from teh table.
    public Cursor getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor data = db.rawQuery(query, null);
        return data;
    }
}
